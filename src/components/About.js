import React from 'react';
import  history from './HistoryService';

const About = (props) => {
    function goTo() {
        history.push('/');
        history.push({ pathname: '/new-place' });
        history.replace({ pathname: '/go-here-instead' });
        history.goBack();
        history.goForward();
        history.go(-3);

    }
    return (
        <div>
            <button onClick={goTo}>click me</button>

            <h1>{props.title}</h1>
        </div>
    );
};

export default About;
