import React from 'react';
import {Link, NavLink} from "react-router-dom";


const Header = () => {
    return (
        <div>
            <ul>
                <li>
                    <NavLink exact to={'/'} activeStyle={{background:'blue'}}>Home</NavLink>
                </li>
                <li>
                    <NavLink to={'/about-us'}>About</NavLink>
                </li>
                <li>
                    <NavLink to={'/courses'}>courses</NavLink>
                </li>
                <li>
                    <NavLink to={'/teachers'}>teachers</NavLink>
                </li>
            </ul>
        </div>
    );
};

export default Header;
