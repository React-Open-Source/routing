import React from 'react';
import {Route,NavLink,Redirect} from "react-router-dom";


import Css from "./Css";
import Javascript from "./Javascript";
import Html from "./Html";

const Courses = ({match}) => {
    return (
        <div>
            <h1>Courses Page</h1>
            <ul>
                <li><NavLink to={`${match.url}/html`}>html </NavLink></li>
                <li><NavLink to={`${match.url}/css`}>css </NavLink></li>
                <li><NavLink to={`${match.url}/javascript`}>javascript</NavLink></li>
            </ul>

            <Route path={'/courses'} render={()=> <Redirect to={'courses/html'} />} />
            <Route path={'/courses/html'}  component={Html}/>
            <Route path={'/courses/css'} component={Css}/>
            <Route path={'/courses/javascript'} component={Javascript}/>
        </div>
    );
};

export default Courses;
