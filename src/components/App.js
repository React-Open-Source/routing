import React from 'react';
import {Router,Route,Switch} from "react-router-dom";
import Home from "./Home";
import About from "./About";
import Header from "./Header";
import Courses from "./cources/Courses";
import Teachers from "./Teachers";
import NotFound from "./NotFound";
import Featured from "./Festured";

import  history from './HistoryService';

const App = () => {
    return (
        <div>
            <Router history={history}>
                <Header/>
                <Switch>
                <Route path={"/"} exact component={Home} />
                <Route path={"/about-us"} render={()=> <About title={'about page is  good'}/>} />
                <Route path={"/courses"} component={Courses} />
                <Route path={"/teachers"} exact component={Teachers} />
                <Route path={"/teachers/:name"} component={Featured} />
                <Route  component={NotFound} />
                </Switch>
            </Router>
        </div>
    );
};

export default App;
